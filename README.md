# Translation teams

Welcome to the GitLab space for the GNOME Translation teams.

Translations are handled on [Damned Lies](https://l10n.gnome.org).
If you want to contribute, [create an account](https://l10n.gnome.org/register/), [find the team for your language](https://l10n.gnome.org/teams/) and join it.
You should find guidelines on your team homepage.

If you want to report an issue with an existing translation, you can use the GitLab project for your language here.
